/**
Author   : Suhendi
Email    : suhendi999@gmail.com
Purpose  : This program is a demonstration (and also visualisation) of Dijkstra's algorithm.
*/
import java.util.*;

public class Point {
 int x, y;
 public Point(int x, int y) {
  this.x = x;
  this.y = y;
 }

}

public static float distanceBetween(Point p1, Point p2) {
  int dx = p1.x - p2.x;
  int dy = p1.y - p2.y;
  return sqrt(dx * dx + dy * dy);
}

public class Map {
  // Size of the map
  int map_width;
  int map_height;
  // Whether or not a tile in the map is traversable.
  boolean[] traversable;
  // Array of all points in this map.
  Point[] points;
  
  public Map(int w, int h) {
    // Initialize the map.
    map_width = w;
    map_height = h;
    // Set all point in the map to be traversable.
    traversable = new boolean[map_width * map_height];
    for (int i = 0; i < map_width * map_height; i++) {
      traversable[i] = true;
    }
    // Initialize all points in this map.
    points = new Point[map_width * map_height];
    for (int y = 0; y < map_height; y++) {
      for (int x = 0; x < map_width; x++) {
        points[x + y * map_width] = new Point(x, y); 
      }
    }
  }
  
  public void setTraversable(int x, int y, boolean t) {
    // Set the traversable condition of a point.
    if (isPointValid(x, y)) {
      traversable[x + y * map_width] = t;
    }
  }
  
  public void setTraversable(Point p, boolean t) {
    // Set the traversable condition of a point.
    setTraversable(p.x, p.y, t);
  }
  
  public Point getPointAt(int x, int y) {
    if (isPointValid(x, y)) {
      return points[x + y * map_width]; 
    }
    return null;
  }
  
  public boolean isPointValid(int x, int y) {
    // Returns true if the point is in the map's scope; otherwise false.
    return (x>=0) && (y>=0) && (x < map_width) && (y < map_height);
  }
  
  public boolean isPointValid(Point p) {
    // Returns true if the point is in the map's scope; otherwise false.
    return isPointValid(p.x, p.y); 
  }
  
  public boolean isPointTraversable(int x, int y) {
    // Returns true if the point is in the map and is traversable; otherwise false.
    return isPointValid(x, y) && traversable[x + y * map_width];
  }
  
  public boolean isPointTraversable(Point p) {
    // Returns true if the point is in the map and is traversable; otherwise false.
    return isPointTraversable(p.x, p.y);
  }
  
  public Point[] neighbourOf(int x, int y) {
    // Returns an array containing all traversable neighbour of a point.
    ArrayList<Point> neighbours = new ArrayList<Point>();
    if (isPointTraversable(x - 1, y)) {
      neighbours.add(getPointAt(x - 1, y)); 
    }
    if (isPointTraversable(x + 1, y)) {
      neighbours.add(getPointAt(x + 1, y)); 
    }
    if (isPointTraversable(x, y - 1)) {
      neighbours.add(getPointAt(x, y - 1)); 
    }
    if (isPointTraversable(x, y + 1)) {
      neighbours.add(getPointAt(x, y + 1)); 
    }
    Point[] retval = new Point[neighbours.size()];
    return neighbours.toArray(retval);
  }
  
  public Point[] neighbourOf(Point p) {
    // Returns an array containing all traversable neighbour of a point.
    return neighbourOf(p.x, p.y);
  }
  
}

// Initialize dijkstra's algorithm.
ArrayList<Point> unvisited = new ArrayList<Point>();
HashMap<Point, Integer> dist = new HashMap<Point, Integer>();
HashMap<Point, Point> prev = new HashMap<Point, Point>();
HashMap<Point, Boolean> visited = new HashMap<Point, Boolean>();
Map map = new Map(20, 20);
Point current;
boolean finished = false;
Point source = map.getPointAt(0, 0);
Point dest = map.getPointAt(15, 7);

void setup() {
  size(500, 500);
  colorMode(RGB, 255);
  frameRate(30);
  surface.setTitle("Dijsktra's Algorithm");
  // Set the map's obstacle.
  for (int i = 3; i <= 7; i++) {
    map.setTraversable(i, 6, false);
  }
  for (int i = 7; i <= 14; i++) {
    map.setTraversable(3, i, false);
  }
  for (int i = 4; i <= 12; i++) {
    map.setTraversable(i, 14, false);
  }
  for (int i = 8; i < 20; i++) {
    map.setTraversable(i, 10, false);
  }
  for (int i = 3; i <= 5; i++) {
    map.setTraversable(7, i, false);
  }
  for (int i = 8; i < 20; i++) {
    map.setTraversable(i, 3, false);
  }
  for (Point p : map.points) {
    dist.put(p, Integer.MAX_VALUE);
    visited.put(p, false);
    unvisited.add(p);
  }
  dist.put(source, 0);
}

void draw() {
  if ((!finished) && (!unvisited.isEmpty())) {
    // Execute one step of dijkstra's algorithm.
    int min = Integer.MAX_VALUE;
    int min_idx = -1;
    for (int i = 0; i < unvisited.size(); i++) {
      if (dist.get(unvisited.get(i)) <= min) {
        min_idx = i;
        min = dist.get(unvisited.get(i));
      }
    }
    Point p = unvisited.get(min_idx);
    if (p == dest) {
      finished = true;
    } else {
      unvisited.remove(min_idx);
      visited.put(p, true);
      for (Point n : map.neighbourOf(p)) {
        int alt = dist.get(p) + 1;
        if (alt < dist.get(n)) {
          dist.put(n, alt);
          prev.put(n, p);
        }
      }
    }
  }
  // Renders the map.
  strokeWeight(0);
  float tile_size = width / map.map_width;
  for (Point p : map.points) {
    if (map.isPointTraversable(p)) {
      if (p == source) {
        fill(255, 128, 128);
      } else if (p == dest) {
        fill(128, 256, 128);
      } else if (visited.get(p)) {
        colorMode(HSB, 255);
        // Set color based on the distance between current point and target point.
        //fill(distanceBetween(p, dest) * 10 % 255, 200, 200);
        // Set  color based on the tentative distance.
        fill(dist.get(p) * 5 % 255, 200, 200);
        colorMode(RGB, 255);
      } else {
        fill(128, 128, 128);
      }
    } else {
      fill(64, 64, 64); 
    }
    rect(p.x * tile_size, p.y * tile_size, tile_size, tile_size);
  }
  // Renders path.
  if (finished) {
    strokeWeight(2);
    noFill();
    Point p = dest;
    beginShape();
    while (p != null) {
      stroke(0, 0, 0);
      vertex((p.x + 0.5f) * tile_size, (p.y + 0.5f) * tile_size);
      p = prev.get(p);
    }
    endShape();
  }
}